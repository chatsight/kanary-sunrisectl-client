const { Base64 } = require('js-base64');
const { spawn } = require("child_process");
const axios = require('axios')
const fs = require('fs')
const schedule = require('node-schedule')
const net = require('net');
const any = require('promise.any');
client = new net.Socket();
any.shim();
module.exports = {
    Base64: Base64,
    spawn: spawn,
    axios: axios,
    fs: fs,
    schedule: schedule,
    net: net,
    any: any,
    client: client
}