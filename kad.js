const { Base64,spawn,axios,fs,schedule,net,client } = require('./dependencies')
var bootstrap = false
var loadout = fs.existsSync("/root/immutableCfg.json") ? require('/root/immutableCfg.json') : bootstrap = true
var relativeTime = 0;

const state = {
    antiEntropy: "00000-00000-00000-00000",
    roundName: "00000-00000-00000-00000",
    roundActive: false,
    shouldRegister: false,
    maxRegisterAttempts: 3,
    currentRegisterAttempts: 0,
    haveRegistered: false,
    shouldUpdate: false,
    attemptingUpdate: false,
    haveUpdated: false,
    sendResult: "success",
    stage: "init",
    hostIP: "127.0.0.1",
    hostPORT: 6015,
    clientPORT: 0
}

var sunriseTCP = net.createServer(function(sock) {sock.end('Sunrise Client\n')});
sunriseTCP.listen(0, function(){state.clientPORT = sunriseTCP.address().port})


const startRelativeTime = async () => {
    console.log("[Sunrise] Relative Internal Clock Started.")
    setInterval(() => {
        relativeTime += 1
    },1000)
}

const getRelativeTime = async (type) => {
    let ct = Math.floor(Date.now() / 1000)
    if(type === "info"){
        console.log("[Sunrise] RTSS - " + relativeTime + " | Accurate - " + ct)
        return;
    }else if(type === "calc"){
        return [relativeTime, ct]
    }
}

const init = () => {
    try{
        if(bootstrap === true){
            var isGCP = new Promise(async(resolve,reject) => {
                try{
                    var xhr = await axios.get('http://metadata.google.internal/computeMetadata/v1/project/',{validateStatus: (s) => s < 500})
                    if(xhr.status === 200){
                        resolve(["gcp","http://metadata.google.internal/computeMetadata/v1/project/"],{validateStatus: (s) => s < 500})
                    }else{
                        reject()
                    }
                }catch(err){
                    reject()
                }
            })
            var isDO = new Promise(async(resolve,reject) => {
                try{
                    var xhr = await axios.get('http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address',{validateStatus: (s) => s < 500})
                    if(xhr.status === 200){
                        resolve([
                             "do",
                            {
                                "base":"http://169.254.169.254/metadata/v1",
                                "ipv4prv":"/interfaces/private/0/ipv4/address",
                                "ipv4pub":"/interfaces/public/0/ipv4/address",
                            }
                        ])
                    }else{
                        reject()
                    }
                }catch(err){
                    reject()
                }
            })
            var isAWS = new Promise(async(resolve,reject) => {
                try{
                    var xhr = await axios.get('http://169.254.169.254/latest/meta-data/',{validateStatus: (s) => s < 500})
                    if(xhr.status === 200){
                        resolve(["aws","http://169.254.169.254/latest/meta-data/"],{validateStatus: (s) => s < 500})
                    }else{
                        reject()
                    }
                }catch(err){
                    reject()
                }
            })
            Promise.any([isGCP,isAWS,isDO]).then(k => {
                const cloud = k
                const immutableFile = {
                    "provider":cloud[0],
                    "base":cloud[1]["base"],
                    "sunrise":"https://sunrise.kanary.dev",
                    "ipv4prv":cloud[1]["ipv4prv"],
                    "ipv4pub":cloud[1]["ipv4pub"]
                }
                fs.writeFileSync('/root/immutableCfg.json', JSON.stringify(immutableFile), 'utf-8')
                loadout = require('/root/immutableCfg.json')
                console.log("[Sunrise] Bootstrap Complete, Client Initalized as type: " + cloud[0])
                state.stage = "waiting"
                startRelativeTime();
            }).catch(k => {
                console.log(k)
                console.log("Unsupported Platform.")
                process.exit(0)
            })
        }else{
            console.log("[Sunrise] Client Initalized.")
            state.stage = "waiting"
            startRelativeTime();
        }
    }catch(err){
        console.log(err)
        console.log("Failed To Initalize.")
        process.exit(0)
    }    
}
init()

const checkUpdates = async () => {
    try{
        let sunrisetoken = await fs.readFileSync('/root/kanary-sunrise/kanary-auth-sunrisekey','utf-8')
        let token = sunrisetoken.replace(/\r?\n|\r/g, '')
        let runRoundCheck = await axios.get(loadout["sunrise"]+'/cli/round/status', {headers: {'x-sunrise-auth': token}},{validateStatus: (s) => s < 500})
        if(runRoundCheck){
            if(runRoundCheck.data[0]["operation"]["active"] === true && state.antiEntropy !== runRoundCheck.data[0]["planning"]["roundName"]){
                console.log("[Sunrise] Update Available. Registering Interest.")
                state.roundActive = true
                state.shouldRegister = true
                state.roundName = runRoundCheck.data[0]["planning"]["roundName"]
                state.antiEntropy = runRoundCheck.data[0]["planning"]["roundName"]
                state.stage = "shouldRegister"
            }
        }
    }catch(err){
        //Retry on next loop. 
        return
    }
} 

const registerForRound = async () => {
    try{
        if(state.currentRegisterAttempts < state.maxRegisterAttempts){
            let clientIpv4Public = await axios.get(loadout.base+loadout.ipv4pub)
            let sunrisetoken = await fs.readFileSync('/root/kanary-sunrise/kanary-auth-sunrisekey','utf-8')
            let token = sunrisetoken.replace(/\r?\n|\r/g, '')
            let clientName = Base64.encode(clientIpv4Public.data+":"+state.clientPORT)
            let runRegister = await axios.get(loadout["sunrise"]+'/update/joinround/'+state.roundName+'/'+clientName, {headers: {'x-sunrise-auth': token}},{validateStatus: (s) => s < 500})
            if(runRegister){
                if(runRegister.status === 200){
                    state.shouldRegister = false
                    state.hostIP = runRegister.data
                    state.haveRegistered = true
                    state.stage = "haveRegistered"
                    console.log("[Sunrise] Registered for Round - " + state.roundName + " as " + clientIpv4Public.data)
                }else{
                    state.currentRegisterAttempts += 1
                }
            }
        }else{
            hostHasDied(null)
        }
    }catch(err){
        state.currentRegisterAttempts += 1
        return
    }        
}

const opportunisticUpdate = async () => {
    try{
        state.attemptingUpdate = true
        let clientIpv4Public = await axios.get(loadout.base+loadout.ipv4pub)
        let sunrisetoken = await fs.readFileSync('/root/kanary-sunrise/kanary-auth-sunrisekey','utf-8')
        let token = sunrisetoken.replace(/\r?\n|\r/g, '')
        let clientName = Base64.encode(clientIpv4Public.data+":"+state.clientPORT)
        let runRegister = await axios.get(loadout["sunrise"]+'/update/package/'+state.roundName+'/'+clientName, {headers: {'x-sunrise-auth': token}},{validateStatus: (s) => s < 500})
        if(runRegister){
            if(runRegister.status === 200){
                let dataSrc = runRegister.data
                let updateTLS = new Promise(async(resolve,reject) => {
                    try{
                        let fetchList = dataSrc.vaultFetch
                        let vaultOneUse = dataSrc.token
                        console.log("[Sunrise] Asked to update. Using OTTP - " + vaultOneUse)
                        for await(const t of fetchList){
                            let request = await axios.get('https://vault.kanary.dev/v1/secret/data/'+t,{headers: {'X-Vault-Token': vaultOneUse}})
                            let datastr = request.data.data.data.cert
                            let dataclean = datastr.replace(/"/g,"").replace(/\\n/g, '\n');
                            fs.writeFileSync('/root/tls/'+t,dataclean,'utf-8')
                        }
                        resolve();
                    }catch(err){
                        reject(err);
                    }
                })

                let updateSelf = new Promise(async(resolve,reject) => {
                    resolve()
                })

                let updateConf = new Promise(async(resolve,reject) => {
                    resolve()
                })
    
                Promise.all([updateTLS]).then( k => {
                    spawn("service",["nomad","restart"])
                    spawn("service",["consul","restart"])
                    state.stage = "monitorHealth"
                }).catch(k => {
                    console.log(k)
                    state.attemptingUpdate = false
                    state.haveUpdated = true
                    state.sendResult = "failure"
                    state.stage = "haveUpdated"
                })
            }
        }
    }catch(err){
        state.attemptingUpdate = false
        //Retry on next loop.
        return
    }
}

const monitorHealth = async () => {
    const query = async () => {
        var sampleDaemon = spawn("systemctl",["show", "nomad.service", "-p", "NRestarts"])
        var failcount = 0
        sampleDaemon.stdout.on('data',(data) => {
            let pre = data.toString();
            let getnumber = pre.replace(/\D/g,'');
            let number = parseInt(getnumber)
            failcount += number
        });
        return new Promise((resolve, reject) => {
            sampleDaemon.stdout.on('end', async function(){
                resolve(failcount > 0)
            })
        });
    }
    let sample = await query()
    if(sample){
        state.attemptingUpdate = false
        state.haveUpdated = true
        state.sendResult = "failure"
        state.stage = "haveUpdated"
    }else{
        state.sendResult = "success"
        state.attemptingUpdate = false
        state.haveUpdated = true
        state.stage = "haveUpdated"     
    }
}

const haveUpdated = async () => {
    let clientIpv4Public = await axios.get(loadout.base+loadout.ipv4pub)
    let sunrisetoken = await fs.readFileSync('/root/kanary-sunrise/kanary-auth-sunrisekey','utf-8')
    let token = sunrisetoken.replace(/\r?\n|\r/g, '')
    let clientName = Base64.encode(clientIpv4Public.data+":"+state.clientPORT)
    try{
        let haveUpdated = await axios.get(loadout["sunrise"]+'/update/sitrep/'+state.roundName+'/'+clientName+'/'+state.sendResult, {headers: {"x-sunrise-auth":token}})
        if(haveUpdated){
            if(haveUpdated.status === 200){
                console.log("[Sunrise] Completed update - " + state.roundName)
                getRelativeTime("info")
                state.roundName = "00000-00000-00000-00000",
                state.roundActive = false,
                state.shouldRegister = false,
                state.haveRegistered = false,
                state.shouldUpdate = false,
                state.haveUpdated = false,
                state.sendResult = "success",
                state.stage = "waiting"
            }else{
                //Retry on next loop.
                return                
            }
        }else{
            //Retry on next loop.
            return
        }
    }catch(err){
        //Retry on next loop.
        return
    }
}

const isHostAlive = async() => {
    client.connect(state.hostPORT, state.hostIP)
}

const hostHasDied = async(stage) => {
    if(stage === "joined"){
        console.log("[Sunrise] Host "+state.hostIP+":"+state.hostPORT+" did not respond to HB. Leaving Round.")
        getRelativeTime()
        state.roundName = "00000-00000-00000-00000",
        state.roundActive = false,
        state.currentRegisterAttempts = 0
        state.shouldRegister = false,
        state.haveRegistered = false,
        state.shouldUpdate = false,
        state.haveUpdated = false,
        state.sendResult = "success",
        state.stage = "waiting"
    }else{
        console.log("[Sunrise] Tried to join new round, but host(s) did not respond to interest.")
        getRelativeTime()
        state.roundName = "00000-00000-00000-00000",
        state.roundActive = false,
        state.shouldRegister = false,
        state.haveRegistered = false,
        state.shouldUpdate = false,
        state.currentRegisterAttempts = 0
        state.haveUpdated = false,
        state.sendResult = "success",
        state.stage = "waiting"
    }    
}

client.on('error', (err) => {
    hostHasDied("joined")
})

client.on('connect', (err) => {
	client.destroy()
})

schedule.scheduleJob('*/5 * * * * *', async function(){

    if(state.roundActive === false && state.stage === "waiting"){
        checkUpdates()
    }

    if(state.shouldRegister === true && state.stage === "shouldRegister"){
        registerForRound()
    }

    if(state.haveRegistered === true && state.stage === "haveRegistered" && state.attemptingUpdate !== true){
        opportunisticUpdate()
    }

    if(state.roundActive === true && state.stage === "monitorHealth"){
        monitorHealth()
    }

    if(state.haveUpdated === true && state.stage === "haveUpdated"){
        haveUpdated()
    }

    if(state.roundActive === true && state.haveRegistered === true){
        isHostAlive()
    }

})

