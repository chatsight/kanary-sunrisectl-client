const { schedule, axios, fs, spawn} = require('../dependencies')
var cfgVersion = ""
var init = true
var sunrisetoken = fs.readFileSync('/root/kanary-sunrise/kanary-auth-sunrisekey','utf-8')
var token = sunrisetoken.replace(/\r?\n|\r/g, '')
var crlf = require('crlf');
var D2UConverter = require('dos2unix').dos2unix

var d2u = new D2UConverter().on('error', function(err) {
    console.error(err);
})


const runInit = async () => {
    try{
        let getCurrentValue = await axios.get('https://sunrise.kanary.dev/update/updatehaproxy/init/null', {headers: {'x-sunrise-auth': token}})
        cfgVersion = getCurrentValue.data
        init = false
        console.log("[Sunrise-Standalone] INIT OK")
        return;
    }catch(err){
        return;
    }
}

const repairCFG = async () => {
    let f = await spawn("sh",[__dirname+"/crlfrepair.sh"])
    if(f){
        console.log("[Sunrise-Standalone] Repairing - "+__dirname+"/crlfrepair.sh")
        return true
    }
}

const checkCFG = async () => {
    const query = async () => {
        var sampleDaemon = spawn("haproxy",["-c", "-V", "-f", "/etc/haproxy/haproxy.cfg"])
        var isValid = false
        sampleDaemon.stdout.on('data',(data) => {
            let pre = data.toString();
            (pre === "Configuration file is valid\n") ? (isValid = true) : (isValid = false)
        });
        return new Promise((resolve, reject) => {
            sampleDaemon.stdout.on('end', async function(){
                resolve(isValid)
            })
        });
    }
    let runQuery = await query()
    if(runQuery){
        return true
    }else{
        return false
    }
}


const checkHAUpdates = async () => {
    try{
        let runRoundCheck = await axios.get('https://sunrise.kanary.dev/update/updatehaproxy/check/'+cfgVersion, {headers: {'x-sunrise-auth': token}})
        if(runRoundCheck.status === 200){
            console.log("[Sunrise-Standalone] Updating - " + cfgVersion)
            fs.writeFileSync('/etc/haproxy/haproxy.cfg',runRoundCheck.data,'utf-8')
            const repairCfgEncoding = await repairCFG()
            if(repairCfgEncoding){
                console.log("[Sunrise-Standalone] Repaired CLRF.")
                //await d2u.process(['/etc/haproxy/haproxy.cfg'])
                const syntaxCheck = await checkCFG()
                if(syntaxCheck){
                    spawn("service",["haproxy","reload"])
                    console.log("[Sunrise-Standalone] Installed new CFG. Reloaded Service.")
                }else{
                    console.log("[Sunrise-Standalone] CFG is invalid. Not adopting this config version.")
                }            
            }    
            let getCurrentValue = await axios.get('https://sunrise.kanary.dev/update/updatehaproxy/init/null', {headers: {'x-sunrise-auth': token}})
            cfgVersion = getCurrentValue.data
            return;
        }
    }catch(err){
        return
    }
} 
schedule.scheduleJob('*/5 * * * * *', async function(){
    if(init === true){
        runInit()
    }else{
        checkHAUpdates()
    }
});